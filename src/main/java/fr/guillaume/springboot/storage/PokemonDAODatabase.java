package fr.guillaume.springboot.storage;

import fr.rathesh.springbook.springbooktest.Pokemon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "database")
public class PokemonDAODatabase implements PokemonDAO {

    @Value("${connectionURL}")
    private String connectionURL;

    public List<Pokemon> getMyPokemons() {
        System.out.println("Using database "+connectionURL);
        List<Pokemon> pokemons = new ArrayList<>();
        try{
            Connection con = DriverManager.getConnection(connectionURL);
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("select * from pokemon");
            while(rs.next()) {
                Pokemon pokemon = new Pokemon();
                pokemon.setId(rs.getInt(1));
                pokemons.add(pokemon);
            }
            con.close();
        }catch(Exception e){ System.out.println(e);}
        return pokemons;
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}

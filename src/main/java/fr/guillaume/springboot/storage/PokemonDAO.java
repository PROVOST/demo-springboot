package fr.guillaume.springboot.storage;

import fr.rathesh.springbook.springbooktest.Pokemon;

import java.util.List;

public interface PokemonDAO {

    public List<Pokemon> getMyPokemons();
}
